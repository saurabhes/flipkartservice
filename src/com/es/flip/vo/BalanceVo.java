package com.es.flip.vo;

import java.util.List;

public class BalanceVo {

	private String statusCode;
	private String statusMessage;
	private List<ClinetsVo> clients;

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public List<ClinetsVo> getClients() {
		return clients;
	}

	public void setClients(List<ClinetsVo> clients) {
		this.clients = clients;
	}

}
