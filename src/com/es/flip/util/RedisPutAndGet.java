package com.es.flip.util;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class RedisPutAndGet {

	public static Boolean saveData(String txnId, String data) {
		MongoClient mongo = null;
		try {
			mongo = new MongoClient(System.getenv("mongo_ip"), 28017);
			MongoDatabase database = mongo.getDatabase("esdb");
			MongoCollection<Document> collection = database.getCollection("flipkart");
			Document document = new Document("txnId", txnId).append("data", data);
			collection.insertOne(document);
			return Boolean.valueOf(true);
		} catch (Exception e) {
			System.out.println("flipKart--- >" + e);
			return Boolean.valueOf(false);
		} finally {
			if (mongo != null) {
				mongo.close();
			}

		}

	}

}
