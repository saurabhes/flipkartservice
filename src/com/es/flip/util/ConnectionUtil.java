package com.es.flip.util;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionUtil {

	/*static String url = "jdbc:mysql://ec2-35-154-249-115.ap-south-1.compute.amazonaws.com:3306/earlysalary";
	static String user = "qa_user";
	static String password = "Qa@2015##$$";*/
	
	
	static String url = System.getenv("dbUrl");
	static String user =System.getenv("userName");
	static String password = System.getenv("password");
	static String driver = "com.mysql.jdbc.Driver";

	public static Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName(driver).newInstance();
			connection = DriverManager.getConnection(url, user, password);
			return connection;
		} catch (Exception e) {
			System.out.println("EarlySalary Connection Exception--- >" + e);
			return connection;
		}
	}
}
