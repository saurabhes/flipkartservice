package com.es.flip.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.es.flip.reqmodel.CardRequestModel;
import com.es.flip.service.CardRequestInterface;
import com.es.flip.service.CardRequestInterfaceImpl;
import com.google.gson.Gson;

public class FlipkartHandler implements RequestHandler<CardRequestModel, Boolean> {

	public Boolean handleRequest(CardRequestModel input, Context context) {
		try {
			CardRequestInterface cardRequestInterface = new CardRequestInterfaceImpl();
			System.out.println("FLipkart Request --->" + new Gson().toJson(input));
			Boolean flag = cardRequestInterface.createCoupon(input);

			if (flag) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
