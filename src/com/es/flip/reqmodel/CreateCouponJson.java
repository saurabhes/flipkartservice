package com.es.flip.reqmodel;

import java.util.List;

public class CreateCouponJson {

	private String transactionId;
	private Recipent recipient;
	private List<Denomination> egvList;

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Recipent getRecipient() {
		return recipient;
	}

	public void setRecipient(Recipent recipient) {
		this.recipient = recipient;
	}

	public List<Denomination> getEgvList() {
		return egvList;
	}

	public void setEgvList(List<Denomination> egvList) {
		this.egvList = egvList;
	}

}
