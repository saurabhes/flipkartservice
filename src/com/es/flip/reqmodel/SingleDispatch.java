package com.es.flip.reqmodel;

public class SingleDispatch {

	private String transactionId;
	private Integer denomination;
	private Recipent recipient;

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Integer getDenomination() {
		return denomination;
	}

	public void setDenomination(Integer denomination) {
		this.denomination = denomination;
	}

	public Recipent getRecipient() {
		return recipient;
	}

	public void setRecipient(Recipent recipient) {
		this.recipient = recipient;
	}

	

}
