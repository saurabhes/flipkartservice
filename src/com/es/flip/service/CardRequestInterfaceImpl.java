package com.es.flip.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import com.es.flip.model.tables.OtpLogDetail;
import com.es.flip.reqmodel.CardRequestModel;
import com.es.flip.reqmodel.CreateCouponJson;
import com.es.flip.reqmodel.Denomination;
import com.es.flip.reqmodel.Recipent;
import com.es.flip.util.ConnectionUtil;
import com.es.flip.util.RedisPutAndGet;
import com.es.flip.vo.BalanceVo;
import com.es.flip.vo.TransectionVo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class CardRequestInterfaceImpl implements CardRequestInterface {

	public Boolean createCoupon(CardRequestModel cardRequestModel) {
		Gson gosn = new Gson();
		String response = null;
		Recipent recipent = null;
		try {
			TransectionVo transectionVo = new Gson().fromJson(sendPost(System.getenv("createTransection")),
					TransectionVo.class);
			System.out.println("Here is Coupon Response And transection Vo -->" + gosn.toJson(transectionVo));
			if (transectionVo != null) {
				System.out.println("Here is Coupon Response -->" + gosn.toJson(transectionVo));
				if (transectionVo.getStatusCode().equalsIgnoreCase("SUCCESS")) {
					transectionVo.setCustomerId(cardRequestModel.getCustomerId().toString());
					transectionVo.setAmount(cardRequestModel.getCouponAmount().toString());
					transectionVo.setEmailId(cardRequestModel.getEmailId());
					transectionVo.setMobileNumber(cardRequestModel.getMobileNumber().toString());
					RedisPutAndGet.saveData(transectionVo.getTransactionId(), gosn.toJson(transectionVo));
					if (cardRequestModel.getCouponAmount() > 50000) {
						CreateCouponJson couponJson = new CreateCouponJson();
						couponJson.setTransactionId(transectionVo.getTransactionId());
						recipent = new Recipent();
						recipent.setEmail(cardRequestModel.getEmailId());
						recipent.setFormat("CSV");
						recipent.setMedium("EMAIL");
						couponJson.setRecipient(recipent);
						couponJson.setEgvList(getDenomination(cardRequestModel.getCouponAmount().intValue()));
						System.out.println("Here-->" + new Gson().toJson(couponJson));
						response = sendPostParameter(System.getenv("createegvList"), couponJson);
					} else {
						CreateCouponJson couponJson = new CreateCouponJson();
						couponJson.setTransactionId(transectionVo.getTransactionId());
						recipent = new Recipent();
						recipent.setEmail(cardRequestModel.getEmailId());
						recipent.setFormat("CSV");
						recipent.setMedium("EMAIL");
						couponJson.setRecipient(recipent);
						couponJson.setEgvList(getDenomination(cardRequestModel.getCouponAmount().intValue()));
						System.out.println("Here-->" + new Gson().toJson(couponJson));
						response = sendPostParameter(System.getenv("createegvList"), couponJson);
					}
					RedisPutAndGet.saveData(transectionVo.getTransactionId(), response);
					String reString = sendGet(System.getenv("getBalance"));
					RedisPutAndGet.saveData(transectionVo.getTransactionId(), reString);
					BalanceVo balanceVo = gosn.fromJson(reString, BalanceVo.class);
					if (balanceVo != null) {
						if (balanceVo.getClients().get(0).getBalance() < Double
								.parseDouble(System.getenv("flipkartAmount"))) {
							String message1 = "Flipkart Coupon Limit is only XXXXXX. Please Maintain Sufficient balance.";
							String finalTwo = message1.replace("XXXXXX",
									balanceVo.getClients().get(0).getBalance().toString());
							Boolean message = couponMessage(finalTwo, "FLIPKART");
							System.out.println("Flipkart Message -->" + message);
						}
					}
					if (response.equalsIgnoreCase("Fail")) {
						return false;
					} else {
						return true;
					}
				} else {
					return false;
				}
			} else {
				return false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	private String sendPost(String urlName) throws Exception {
		try {
			StringBuffer response = null;
			URL obj = new URL(urlName);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.addRequestProperty("Flipkart-Gifting-Client-Id", System.getenv("clientId"));
			con.addRequestProperty("Flipkart-Gifting-Client-Token", System.getenv("clientToken"));
			con.setDoOutput(true);
			int responseCode = con.getResponseCode();
			System.out.println("Post parameters : " + urlName);
			System.out.println("Response Code : " + responseCode);
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			if (responseCode == 200 || responseCode == 201) {
				response = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				System.out.println(response.toString());
				return response.toString();
			} else {
				return "Fail";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "Fail";
		}

	}

	private String sendGet(String urlName) throws Exception {
		try {

			URL obj = new URL(urlName);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			con.addRequestProperty("Flipkart-Gifting-Client-Id", System.getenv("clientId"));
			con.addRequestProperty("Flipkart-Gifting-Client-Token", System.getenv("clientToken"));
			con.setDoOutput(true);
			int responseCode = con.getResponseCode();
			System.out.println("Post parameters : " + urlName);
			System.out.println("Response Code : " + responseCode);
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			if (responseCode == 200 || responseCode == 201) {
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				System.out.println(response.toString());
				return response.toString();
			} else {
				return "Fail";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "Fail";
		}

	}

	private String sendPostParameter(String urlName, Object couponJson) throws Exception {
		Gson gsonObj = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
		String jsonPayLoad = gsonObj.toJson(couponJson);
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(urlName);

		StringEntity entity = null;
		try {
			entity = new StringEntity(jsonPayLoad);
		} catch (UnsupportedEncodingException ex) {
			ex.printStackTrace();
		}
		httpPost.setEntity(entity);
		httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("Content-type", "application/json");
		httpPost.setHeader("Flipkart-Gifting-Client-Id", System.getenv("clientId"));
		httpPost.setHeader("Flipkart-Gifting-Client-Token", System.getenv("clientToken"));
		CloseableHttpResponse response = null;
		String responseString = null;

		try {

			response = client.execute(httpPost);
			System.out.println("---- Resonse :" + response.getStatusLine().getStatusCode());
			if (response.getStatusLine().getStatusCode() == 200 || response.getStatusLine().getStatusCode() == 201) {
				System.out.println("---- Resonse :" + response.getStatusLine().getReasonPhrase());
				responseString = EntityUtils.toString(response.getEntity(), "UTF-8");
				System.out.println(responseString);
				return responseString;
			} else {
				return "Fail";
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return "Fail";
		}

	}

	private List<Denomination> getDenomination(Integer amount) {
		Denomination denomination = null;
		List<Denomination> denominations = new ArrayList<Denomination>();
		int den[] = { 10000, 5000, 2000, 1000 };
		int totalNotes = 0, count = 0;
		System.out.println("\nDENOMINATIONS: \n");
		for (int i = 0; i < 4; i++) {
			denomination = new Denomination();
			count = amount / den[i];
			if (count != 0) {
				denomination.setDenomination(den[i]);
				denomination.setQuantity(count);
				System.out.println(den[i] + "\tx\t" + count + "\t= " + den[i] * count);
				denominations.add(denomination);
			}
			totalNotes = totalNotes + count;
			amount = amount % den[i];

		}
		return denominations;
	}

	private static Boolean couponMessage(String sms, String type) {
		DSLContext dslContext = null;
		Connection connection = null;
		try {
			connection = ConnectionUtil.getConnection();
			dslContext = DSL.using(connection, SQLDialect.MYSQL);
			String amazon[] = System.getenv("amazonSms").split(",");
			List<Long> longs = new ArrayList<Long>();
			for (String string : amazon) {
				longs.add(Long.parseLong(string));
			}
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String sql = "select * from otp_log_detail where date(created_on)='" + dateFormat.format(new Date())
					+ "'and type='" + type + "'";
			Result<Record> result = dslContext.fetch(sql);
			System.out.println("Query Flipkart" + sql);
			if (result.isEmpty()) {
				for (String string : amazon) {
					URL url = new URL(System.getenv("smsUrl") + string + "&Text=" + URLEncoder.encode(sms, "UTF-8"));
					HttpURLConnection connection1 = (HttpURLConnection) url.openConnection();
					System.out.println("URL .. -> " + url.toString());
					connection1.setDoOutput(true);
					connection1.setConnectTimeout(1000);
					connection1.setReadTimeout(1000);
					System.out.println("Here is SMS Flipkart Respones" + connection1.getResponseCode());
					BufferedReader in = new BufferedReader(new InputStreamReader(connection1.getInputStream()));
					String inputLine;
					StringBuffer response = new StringBuffer();

					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();
					System.out.println("SMS Response Flipakart-->" + response.toString());
					dslContext
							.insertInto(OtpLogDetail.OTP_LOG_DETAIL, OtpLogDetail.OTP_LOG_DETAIL.MDN,
									OtpLogDetail.OTP_LOG_DETAIL.EXPIRE_ON, OtpLogDetail.OTP_LOG_DETAIL.TYPE)
							.values(Long.parseLong(string), new java.sql.Timestamp(new Date().getTime()), type)
							.execute();
				}

			}
			return true;
		} catch (Exception e) {
			System.out.println("Exception --- Coupon Message");
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (connection != null) {
					dslContext.close();
					connection.close();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
