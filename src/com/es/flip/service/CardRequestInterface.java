package com.es.flip.service;

import com.es.flip.reqmodel.CardRequestModel;

public interface CardRequestInterface {

	Boolean createCoupon(CardRequestModel cardRequestModel);

}
